﻿using BankService.Web.Models;
using BankService.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BankService.Web.Controllers
{
    public class AccountController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IAccountService _accountService;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager ,IAccountService accountService, IService service)
            : base(service)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _accountService = accountService;
        }

        [HttpGet]
        public IActionResult Login( string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model ,string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (ModelState.IsValid)
            {
                if (model.AccountNumber == _service.GetFirstInvoiceByUserName(model.UserName))
                {
                    if (_accountService.Login(model))
                    {
                        var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.SecureLogin, false);
                        await _userManager.SetTwoFactorEnabledAsync(_service.GetUserByUserName(model.UserName), model.SecureLogin);
                        if (result.Succeeded)
                        {

                            return RedirectToLocal(returnUrl, model);
                        }
                    }
                    
                }
                
                

                ModelState.AddModelError("", "Bejelentkezés sikertelen!");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Register(string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.UserName , PinCode = _accountService.Register(model), FullName = model.FullName};
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToLocal(returnUrl);
                }

                ModelState.AddModelError("", "Sikertelen regisztráció!");
            }

            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


        private IActionResult RedirectToLocal(String returnUrl, LoginViewModel model)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(InvoicesController.Index), "Invoices", new { UserName = model.UserName });
            }
        }

        private IActionResult RedirectToLocal(String returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(InvoicesController.Index), "Invoices");
            }
        }


        [HttpGet]
        public IActionResult SecureLogin(string UserName, string id, Int32 Amount)
        {
            ViewBag.UserName = UserName;
            ViewBag.Id = id;
            ViewBag.Amount = Amount;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SecureLogin(SecureLoginViewModel model, string UserName, string id, Int32 Amount)
        {
            ViewBag.UserName = UserName;
            ViewBag.Id = id;
            ViewBag.Amount = Amount;
            model.AccountNumber = id;
            model.Amount = Amount;
            var result = await _signInManager.PasswordSignInAsync(UserName, model.Password, true, false);
            if (result.Succeeded && Amount == 0)
            {
                return RedirectToAction(nameof(InvoicesController.InvoiceHistoryWithSecureLogin), "Invoices", new { id = model.AccountNumber });
            }
            if (result.Succeeded && Amount != 0)
            {
                return RedirectToAction(nameof(InvoicesController.TransferWithSecureLogin), "Invoices", new { id = model.AccountNumber , amount = model.Amount});
            }


            return View(model);
        }

        

        public IActionResult Index()
        {
            return View();
        }
    }
}
