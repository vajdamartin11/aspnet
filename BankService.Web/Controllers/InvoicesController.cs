﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BankService.Web.Models;
using BankService.Web.Services;
using Microsoft.AspNetCore.Mvc.Routing;

namespace BankService.Web.Controllers
{
    public class InvoicesController : BaseController
    {
        private readonly BankServiceDbContext _context;
        public InvoicesController(IService service,BankServiceDbContext context)
            :base(service)
        {
            _context = context;
        }

        
        public async Task<IActionResult> Index(String UserName)
        {
            
            if (UserName == "")
            {
                NotFound();
            }
            var bankServiceDbContext = _context.Invoices.Where(i => i.UserName == UserName).Include(i => i.User);
            return View(await bankServiceDbContext.ToListAsync());
        }

        // GET: Invoices/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var invoice = await _context.Invoices
                .Include(i => i.User)
                .FirstOrDefaultAsync(m => m.AccountNumber == id);
            if (invoice == null)
            {
                return NotFound();
            }

            return View(invoice);
        }

        // GET: Invoices/Create
        public async Task<IActionResult> InvoiceHistory(string id)
        {
            if (id == "")
            {
                NotFound();
            }
            ViewBag.id = id;
            

            var bankServiceDbContext = _context.Transactions.Where(t => (t.UserAccountNumberFrom == id || t.UserAccountNumberTo == id) && t.Date.AddDays(30) >= DateTime.Now);
            if (bankServiceDbContext == null)
            {
                return NotFound();
            }
            return View(await bankServiceDbContext.ToListAsync());
        }
        public async Task<IActionResult> InvoiceHistoryWithSecureLogin(string id)
        {
            if (id == "")
            {
                NotFound();
            }
            ViewBag.id = id;

            var bankServiceDbContext = _context.Transactions.Where(t => (t.UserAccountNumberFrom == id || t.UserAccountNumberTo == id) && t.Date.AddDays(30) >= DateTime.Now);
            if (bankServiceDbContext == null)
            {
                return NotFound();
            }
            return View(await bankServiceDbContext.ToListAsync());
        }



        public IActionResult Transfer(Int32 Amount, string id)
        {
            ViewBag.id = id;
            ViewBag.Amount = Amount;
            return View();
        }

        

        // POST: Transactions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Transfer([Bind("UserAccountNumberFrom,UserAccountNumberTo,Type,FullNameTo,Date,Amount")] Transaction transaction, Int32 Amount, string id, DateTime date)
        {
            ViewBag.id = id;
            ViewBag.Amount = Amount;
            ViewBag.date = date;
            transaction.Date = date;
            
            if (!InvoiceExists(transaction.UserAccountNumberFrom))
                ModelState.AddModelError("", "Sikertelen tranzakció!");

            if (_service.IsLocked(id))
                ModelState.AddModelError("", "Sikertelen tranzakció! A számla zárolva van!");

            if (Amount <= 0 || Amount > _service.GetInvoiceAmount(id))
                ModelState.AddModelError("", "Sikertelen tranzakció! Az átutalandó összeg nem megfelelő, kérem ellenőrizze!");

            if (ModelState.IsValid)
            {
                _service.DecreaseInvoiceAmount(transaction.UserAccountNumberFrom, Amount);
                if (InvoiceExists(transaction.UserAccountNumberTo))
                    _service.IncreaseInvoiceAmount(transaction.UserAccountNumberTo, Amount);
                _context.Add(transaction);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Invoices", new { id = id });
            }
            return View(transaction);
        }



        public IActionResult TransferWithSecureLogin(string id, Int32 amount)
        {
            ViewBag.id = id;
            ViewBag.Amount = amount;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TransferWithSecureLogin([Bind("UserAccountNumberFrom,UserAccountNumberTo,Type,FullNameTo,Date,Amount")] Transaction transaction, Int32 Amount, string id, DateTime date)
        {
            ViewBag.id = id;
            ViewBag.Amount = Amount;
            ViewBag.date = date;
            transaction.Date = date;

            if (!InvoiceExists(transaction.UserAccountNumberFrom))
                ModelState.AddModelError("", "Sikertelen tranzakció!");

            if (_service.IsLocked(id))
                ModelState.AddModelError("", "Sikertelen tranzakció! A számla zárolva van!");

            if (Amount <= 0 || Amount > _service.GetInvoiceAmount(id))
                ModelState.AddModelError("", "Sikertelen tranzakció! Az átutalandó összeg nem megfelelő, kérem ellenőrizze!");

            if (ModelState.IsValid)
            {
                _service.DecreaseInvoiceAmount(transaction.UserAccountNumberFrom, Amount);
                if (InvoiceExists(transaction.UserAccountNumberTo))
                    _service.IncreaseInvoiceAmount(transaction.UserAccountNumberTo, Amount);
                _context.Add(transaction);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Invoices", new { id = id });
            }
            return View(transaction);
        }

        private bool InvoiceExists(string id)
        {
            return _context.Invoices.Any(e => e.AccountNumber == id);
        }
    }
}
