﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BankService.Migrations
{
    public partial class fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberTo",
                table: "Transactions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserAccountNumberTo",
                table: "Transactions",
                column: "UserAccountNumberTo");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberTo",
                table: "Transactions",
                column: "UserAccountNumberTo",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberTo",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserAccountNumberTo",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberTo",
                table: "Transactions",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
