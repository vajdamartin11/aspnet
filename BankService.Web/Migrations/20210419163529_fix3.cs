﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BankService.Migrations
{
    public partial class fix3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "PinCode",
                table: "User",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PinCode",
                table: "User",
                type: "int",
                nullable: false,
                oldClrType: typeof(byte[]));
        }
    }
}
