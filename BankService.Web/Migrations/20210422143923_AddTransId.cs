﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BankService.Migrations
{
    public partial class AddTransId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberFrom",
                table: "Transactions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Transactions",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberFrom",
                table: "Transactions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddColumn<int>(
                name: "TransactionId",
                table: "Transactions",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Transactions",
                table: "Transactions",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserAccountNumberFrom",
                table: "Transactions",
                column: "UserAccountNumberFrom");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberFrom",
                table: "Transactions",
                column: "UserAccountNumberFrom",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberFrom",
                table: "Transactions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Transactions",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserAccountNumberFrom",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "TransactionId",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberFrom",
                table: "Transactions",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Transactions",
                table: "Transactions",
                column: "UserAccountNumberFrom");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberFrom",
                table: "Transactions",
                column: "UserAccountNumberFrom",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
