﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BankService.Migrations
{
    public partial class TransFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberFrom",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberTo",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserAccountNumberFrom",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserAccountNumberTo",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberTo",
                table: "Transactions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberFrom",
                table: "Transactions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceFromAccountNumber",
                table: "Transactions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceToAccountNumber",
                table: "Transactions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_InvoiceFromAccountNumber",
                table: "Transactions",
                column: "InvoiceFromAccountNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_InvoiceToAccountNumber",
                table: "Transactions",
                column: "InvoiceToAccountNumber");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_InvoiceFromAccountNumber",
                table: "Transactions",
                column: "InvoiceFromAccountNumber",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_InvoiceToAccountNumber",
                table: "Transactions",
                column: "InvoiceToAccountNumber",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_InvoiceFromAccountNumber",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Invoices_InvoiceToAccountNumber",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_InvoiceFromAccountNumber",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_InvoiceToAccountNumber",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "InvoiceFromAccountNumber",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "InvoiceToAccountNumber",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberTo",
                table: "Transactions",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserAccountNumberFrom",
                table: "Transactions",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserAccountNumberFrom",
                table: "Transactions",
                column: "UserAccountNumberFrom");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserAccountNumberTo",
                table: "Transactions",
                column: "UserAccountNumberTo");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberFrom",
                table: "Transactions",
                column: "UserAccountNumberFrom",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Invoices_UserAccountNumberTo",
                table: "Transactions",
                column: "UserAccountNumberTo",
                principalTable: "Invoices",
                principalColumn: "AccountNumber",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
