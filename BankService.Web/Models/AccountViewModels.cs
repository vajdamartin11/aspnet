﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Models
{
    public class LoginViewModel
    {
        [DisplayName("Név")]
        [Required]
        public string UserName { get; set; }

        [DisplayName("Számlaszám")]
        [Required]
        [RegularExpression("[0-9]{8}-[0-9]{8}$", ErrorMessage = "A számlaszám formátuma, vagy hossza nem megfelelő.")]
        public string AccountNumber { get; set; }

        [Required]
        [DisplayName("Jelszó")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DisplayName("Pin")]
        [DataType(DataType.Password)]
        public String PinCode { get; set; }

        /// <summary>
	    /// Bejelentkezés megjegyzése.
	    /// </summary>
	    public Boolean SecureLogin { get; set; }
    }

    public class RegisterViewModel
    {
        [DisplayName("Felhasználónév")]
        [Required]
        public string UserName { get; set; }

        [DisplayName("Teljes Név")]
        [Required]
        public string FullName { get; set; }

        [Required]
        [DisplayName("Jelszó")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DisplayName("Jelszó megerősítése")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string PasswordRepeat { get; set; }

        [Required]
        [DisplayName("Pin")]
        public String PinCode { get; set; }

        [Required]
        [DisplayName("Pin megerősítése")]
        [Compare("PinCode")]
        public String PinCodeRepeat { get; set; }
    }

    public class SecureLoginViewModel
    {

        public Int32 Amount { get; set; }
        public string AccountNumber { get; set; }
        [Required]
        [DisplayName("Jelszó")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
