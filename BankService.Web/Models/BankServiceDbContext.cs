﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Models
{
    public class BankServiceDbContext : IdentityDbContext<User,IdentityRole<int>, int>
    {

        //public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        //public DbSet<Employee> Employees { get; set; }

        public BankServiceDbContext(DbContextOptions<BankServiceDbContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("User").Property(x => x.Id).HasColumnName("UserId");

        }
    }
}
