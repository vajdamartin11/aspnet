﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Models
{
    public class DbInitializer
    {
        public static void Initialize(BankServiceDbContext context)
        {
            // Use only one of the below.
            // Create / update database based on migration classes.
            context.Database.Migrate();
            // Create database if not exists based on current code-first model (no migrations!).
            //context.Database.EnsureCreated();

            /*if (context.Users.Any())
            {
                return;
            }*/



            /*var invoices = new Invoice[]
            {
                new Invoice
                {
                    UserId = 1,
                    AccountNumber = "11783945-19492494",
                    CreationDate = new DateTime(2011,6,13,15,0,0),
                    UserName = "vmartinis",
                },
                new Invoice
                {
                    UserId = 1,
                    AccountNumber = "11783945-19492495",
                    CreationDate = new DateTime(2013,4,10,15,0,0),
                    UserName = "vmartinis",
                },
                new Invoice
                {
                    UserId = 2,
                    AccountNumber = "22222222-22222222",
                    CreationDate = new DateTime(2013,4,10,15,0,0),
                    UserName = "tesztjoska",
                },
                new Invoice
                {
                    UserId = 2,
                    AccountNumber = "22222222-22222223",
                    CreationDate = new DateTime(2011,6,13,15,0,0),
                    UserName = "tesztjoska",
                },
                
            };
            foreach (Invoice i in invoices)
            {
                context.Invoices.Add(i);
            }

            context.SaveChanges();*/

            /*var transactions = new Transaction[]
            {
                new Transaction
                {
                    UserAccountNumberFrom = "",
                    UserAccountNumberTo = "11783945-19492494",
                    Type = TransactionType.Deposit,
                    FullNameTo = "Vajda Martin István",
                    Date = new DateTime(2021,4,1,15,0,0),
                    Amount = 20000,
                },
                new Transaction
                {
                    UserAccountNumberFrom = "22222222-22222222",
                    UserAccountNumberTo = "11783945-19492494",
                    Type = TransactionType.Transfer,
                    FullNameTo = "Vajda Martin István",
                    Date = new DateTime(2021,3,30,18,0,0),
                    Amount = 50000,
                },
                new Transaction
                {
                    UserAccountNumberFrom = "",
                    UserAccountNumberTo = "11783945-19492494",
                    Type = TransactionType.Deposit,
                    FullNameTo = "Vajda Martin István",
                    Date = new DateTime(2021,3,1,15,0,0),
                    Amount = 20000,
                },
                
            };
            foreach (Transaction t in transactions)
            {
                context.Transactions.Add(t);
            }

            context.SaveChanges();*/
        }

        
    }
}
