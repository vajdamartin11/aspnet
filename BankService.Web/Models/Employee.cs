﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Models
{
    public class Employee : IdentityUser<int>
    {
        public Employee()
        {
            Invoices = new HashSet<Invoice>();
        }
        public ICollection<Invoice> Invoices { get; set; }
    }
}
