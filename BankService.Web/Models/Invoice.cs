﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Models
{
    public class Invoice
    {
        [Key]
        public String AccountNumber { get; set; }
        [ForeignKey("User")]
        public Int32 UserId { get; set; }
        public DateTime CreationDate { get; set; }
        public String UserName { get; set; }
        public Boolean Locked { get; set; }
        public Int32 Amount { get; set; }
        [Required]
        public User User { get; set; }

        [InverseProperty("InvoiceFrom")]
        public virtual ICollection<Transaction> TransactionsSend { get; set; }
        [InverseProperty("InvoiceTo")]
        public virtual ICollection<Transaction> TransactionsGet { get; set; }

    }
}
