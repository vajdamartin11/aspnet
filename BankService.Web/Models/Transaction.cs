﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Models
{
    public enum TransactionType
    {
        Transfer,

        Deposit,

        Withdraw
    }
    public class Transaction
    {
        [Key]
        public Int32 TransactionId { get; set; }
        public String UserAccountNumberFrom { get; set; }
        
        [RegularExpression("[0-9]{8}-[0-9]{8}$", ErrorMessage = "A számlaszám formátuma, vagy hossza nem megfelelő.")]
        public String UserAccountNumberTo { get; set; }

        public TransactionType Type { get; set; }

        public String FullNameTo { get; set; }

        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Az összegnek többnek kell lennie 1-nél, vagy kevesebbnek a számlán lévő összegnél")]
        public Int32 Amount { get; set; }
        

        public virtual Invoice InvoiceFrom{ get; set; }
        public virtual Invoice InvoiceTo { get; set; }



    }
}
