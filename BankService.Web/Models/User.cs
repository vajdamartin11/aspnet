﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace BankService.Web.Models
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            Invoices = new HashSet<Invoice>();
        }
        [Required]
        public String FullName { get; set; }
        
        [Required]
        public byte[] PinCode { get; set; }

        public ICollection<Invoice> Invoices { get; set; }


    }
}
