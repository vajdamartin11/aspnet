﻿using BankService.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BankService.Web.Services
{
    public class AccountService : IAccountService
    {
        private readonly BankServiceDbContext _context;

        public AccountService(BankServiceDbContext context)
        {
            _context = context;
        }

        public User GetUser(String userName)
        {
            if (userName == null)
                return null;

            return _context.Users.FirstOrDefault(c => c.UserName == userName); // megkeressük a vendéget
        }

        public Boolean Login(LoginViewModel user)
        {
            if (user == null)
                return false;

            // ellenőrizzük az annotációkat
            if (!Validator.TryValidateObject(user, new ValidationContext(user, null, null), null))
                return false;

            User tUser = _context.Users.FirstOrDefault(c => c.UserName == user.UserName); // megkeressük a felhasználót

            if (tUser == null)
                return false;

            // ellenőrizzük a jelszót (ehhez a kapott jelszót hash-eljük)
            Byte[] passwordBytes = null;
            using (SHA512CryptoServiceProvider provider = new SHA512CryptoServiceProvider())
            {
                passwordBytes = provider.ComputeHash(Encoding.UTF8.GetBytes(user.PinCode));
            }

            if (!passwordBytes.SequenceEqual(tUser.PinCode))
                return false; 
            if (!(_context.Invoices.FirstOrDefault(c => c.UserId == tUser.Id).AccountNumber == user.AccountNumber))
                return false;

            return true;
        }
        public byte[] Register(RegisterViewModel user)
        {
            // kódoljuk a jelszót
            Byte[] passwordBytes = null;
            using (SHA512CryptoServiceProvider provider = new SHA512CryptoServiceProvider())
            {
                passwordBytes = provider.ComputeHash(Encoding.UTF8.GetBytes(user.PinCode));
            }

            return passwordBytes;
        }
    }
}
