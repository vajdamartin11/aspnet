﻿using BankService.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Services
{
    public interface IAccountService
    {
        User GetUser(String userName);

        Boolean Login(LoginViewModel user);

        byte[] Register(RegisterViewModel user);
    }
}
