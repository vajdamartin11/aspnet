﻿using BankService.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Services
{
    public interface IService
    {
        public User GetUserByUserName(String username);

        public List<Invoice> GetInvoicesByUserName(String username);
        public String GetFirstInvoiceByUserName(String username);
        public void IncreaseInvoiceAmount(String AccountNumber, Int32 Amount);

        public void DecreaseInvoiceAmount(String AccountNumber, Int32 Amount);

        public bool IsLocked(String AccountNumber);

        public Int32 GetInvoiceAmount(String AccountNumber);

        public void AddInvoiceToUser(Invoice invoic);
    }
}
