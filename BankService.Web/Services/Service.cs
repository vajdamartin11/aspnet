﻿using BankService.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankService.Web.Services
{
    public class Service : IService
    {
        private readonly BankServiceDbContext _context;

        public Service(BankServiceDbContext context)
        {
            _context = context;
        }

        public User GetUserByUserName(String username)
        {
            return _context.Users
                .Single(u => u.UserName == username); // throws exception if id not found
        }

        public List<Invoice> GetInvoicesByUserName(String username)
        {
            return _context.Invoices
                 .Where(i => i.UserName == username)
                 .ToList();
        }
        public String GetFirstInvoiceByUserName(String username)
        {
            return _context.Invoices
                 .Where(i => i.UserName == username)
                 .FirstOrDefault()
                 .AccountNumber;
        }

        public void IncreaseInvoiceAmount(String AccountNumber, Int32 Amount)
        {
            _context.Invoices
               .Where(i => i.AccountNumber == AccountNumber).FirstOrDefault().Amount += Amount;
            _context.SaveChanges();
        }
        public void DecreaseInvoiceAmount(String AccountNumber, Int32 Amount)
        { 
            _context.Invoices
               .Where(i => i.AccountNumber == AccountNumber).FirstOrDefault().Amount -= Amount;
            _context.SaveChanges();
        }

        public bool IsLocked(String AccountNumber)
        {
            return _context.Invoices
               .Where(i => i.AccountNumber == AccountNumber).FirstOrDefault().Locked;
        }

        public Int32 GetInvoiceAmount(String AccountNumber)
        {
            return _context.Invoices
               .Where(i => i.AccountNumber == AccountNumber).FirstOrDefault().Amount;
        }
        public void AddInvoiceToUser(Invoice invoice)
        {
            if (invoice == null)
                throw new ArgumentNullException(nameof(invoice), "The invoice to add must not be null.");

            _context.Invoices.Add(invoice);
            _context.SaveChanges();
        }
    }
}
